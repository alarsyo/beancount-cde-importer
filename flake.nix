{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix.url = "github:nix-community/poetry2nix";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ poetry2nix.overlay ];
        };
        poetryEnv = pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
        };
      in
        {
          packages.beancount-cde-importer = pkgs.poetry2nix.mkPoetryApplication {
            projectDir = ./.;
          };
          defaultPackage = self.packages.${system}.beancount-cde-importer;

          devShell = pkgs.mkShell {
            buildInputs =  [
              poetryEnv

              pkgs.poetry
            ];
          };
        });
}
